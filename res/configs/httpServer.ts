const config = {
    port: process.env.APPLICATION_PORT,
};

export default config;
