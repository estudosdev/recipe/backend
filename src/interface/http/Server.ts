import express, { Application } from 'express';

type PropsConfig = {
    port: string,
    subtitle: string,
};

interface IServer {
    createServer(): void;
}

class Server implements IServer {
    protected app: Application;
    private config: PropsConfig;

    constructor({ httpServerConfig }) {
        this.config = httpServerConfig;
        this.app = express();
    }

    public createServer(): void {
        const { port } = this.config;
        this.app.listen(Number(port));
    }
}

export default Server;
