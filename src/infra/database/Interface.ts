export interface IDatabase {
    authenticate(): Promise<any>;
}
