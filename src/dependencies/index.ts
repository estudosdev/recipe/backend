import {
    createContainer,
    asClass,
    asValue,
} from 'awilix';

const container = createContainer();

import httpServerConfig from '../../res/configs/httpServer';

import Application from '../Application';
import HttpServer from '../interface/http/Server';

container.register({
    httpServerConfig: asValue(httpServerConfig),
});

container.register({
    httpServer: asClass(HttpServer).singleton(),
    application: asClass(Application).singleton(),
});

export default container;
