export interface IApplication {
    start(): Promise<any>;
}

class Application implements IApplication {
    private httpServer;

    constructor({ httpServer }) {
        this.httpServer = httpServer;
    }

    public async start(): Promise<any> {
        console.log(this.httpServer);
        return new Promise((resolve) => {
            resolve(console.log('Running...'));
        });
    }
}

export default Application;
