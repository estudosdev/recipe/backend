export interface IRecipe {
    execute(): void;
}

export type PropsRecipe = {
    name: string,
    description: string,
};
