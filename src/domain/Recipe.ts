import { IRecipe, PropsRecipe } from './IRecipe';

class Recipe implements IRecipe {
    private name: string;
    private description: string;

    constructor(props: PropsRecipe) {
        this.name = props.name;
        this.description = props.description;
    }

    public execute() {
        return this.name + this.description;
    }
}

export default Recipe;
