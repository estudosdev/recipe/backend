import container from './dependencies';
import { IApplication } from './Application';

const application: IApplication = container.resolve('application');

application.start();
