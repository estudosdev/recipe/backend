## User Stories

-   [ ] User can see a list of recipe titles
-   [ ] User can click a recipe title to display a recipe card containing the
recipe title, meal type (breakfast, lunch, supper, or snack), number of people
it serves, its difficulty level (beginner, intermediate, advanced), the list
of ingredients (including their amounts), and the preparation steps.
-   [ ] User click a new recipe title to replace the current card with a new
recipe.
