import Recipe from '../../../src/domain/Recipe';
import validaPayload from '../../support/validPayloadRecipe';
import { normalizeReturnClass } from './helper';

describe('DOMAIN ::: RECIPE', () => {
    test('Valid payload to domain', () => {
        const recipe = new Recipe(validaPayload);
        expect(normalizeReturnClass(recipe)).toBe(normalizeReturnClass({
            name: 'Bolo de abacaxi',
            description: '1 colher de açúcar e 2 copos de água',
        }));
    });
});
