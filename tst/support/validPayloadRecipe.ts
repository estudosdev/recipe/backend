import {
    PropsRecipe
} from '../../src/domain/IRecipe';

const payload: PropsRecipe = {
    name: 'Bolo de abacaxi',
    description: '1 colher de açúcar e 2 copos de água',
};

export default payload;
